// / <reference types="cypress" />

// @use-case/user-registration @web
describe('Feature: Enable Users authentication', () => {
  // @persona/kevin @nominal
  it(`Scenario: On Web app, Users could authenticate on /auth/login route
When Kevin authenticates himself
Then Kevin is redirected to the home page
And Kevin is able to see latest posts on home page
And Kevin is logged in`, () => {
    cy.intercept('POST', '/auth/login', {
      statusCode: 200,
      body: {
        user: {
          userId: 1,
        },
        token: 'qwertyuiop',
      },
    }).as('api-login')
    cy.login('another-username', 'another-password')
    cy.url().should('equal', 'http://localhost:3000/')
    cy.get('[aria-describedby="posts list"]').should('exist')
  })

  // @persona/kevin @rule/username-required @bad-request
  it(`Scenario: On Web app, Users cannot authenticate without a username
When Kevin authenticates himself and ommit username
And Kevin is not logged in
And Kevin is still on login page
And Kevin is warned that the username field is required`, () => {
    cy.intercept('POST', '/auth/login', {
      statusCode: 400,
      body: {
        code: 'rule/username-required',
        details: 'username is required for registration',
      },
    }).as('api-login')
    cy.login(undefined, 'another-password')
    cy.url().should('equal', 'http://localhost:3000/auth/login')
    cy.get('[aria-describedby="username error"]')
      .should('exist')
      .and('be.visible')
      .and('contain.text', 'required')
  })

  // @persona/kevin @rule/user-registered @not-found
  it(`Scenario: On Web app, Users cannot authenticate with unregistered username
When Kevin authenticates himself and provides unregistered username
And Kevin is not logged in
And Kevin is still on login page
And Kevin is warned that the username is not registered`, () => {
    cy.intercept('POST', '/auth/login', {
      statusCode: 404,
      body: {
        code: 'rule/user-registered',
        details: 'user not found by username',
      },
    }).as('api-login')
    cy.login('another-username', 'another-password')
    cy.url().should('equal', 'http://localhost:3000/auth/login')
    cy.get('[aria-describedby="username error"]')
      .should('exist')
      .and('be.visible')
      .and('contain.text', 'not found')
  })

  // @persona/kevin @rule/password-required @bad-request
  it(`Scenario: On Web app, Users cannot authenticate without password
When Kevin authenticates himself and ommit password
And Kevin is not logged in
And Kevin is still on login page
And Kevin is warned that the password field is required`, () => {
    cy.intercept('POST', '/auth/login', {
      statusCode: 400,
      body: {
        code: 'rule/password-required',
        details: 'password is required for registration',
      },
    }).as('api-login')
    cy.login(undefined, 'another-password')
    cy.url().should('equal', 'http://localhost:3000/auth/login')
    cy.get('[aria-describedby="password error"]')
      .should('exist')
      .and('be.visible')
      .and('contain.text', 'required')
  })

  // @persona/kevin @rule/password-match @bad-request
  it(`Scenario: On Web app, Users cannot authenticate with bad password
When Kevin authenticates himself and provides bad password
And Kevin is not logged in
And Kevin is still on login page
And Kevin is warned that the password did not match`, () => {
    cy.intercept('POST', '/auth/login', {
      statusCode: 400,
      body: {
        code: 'rule/password-match',
        details: 'user password mismatch',
      },
    }).as('api-login')
    cy.login('another-username', 'another-password')
    cy.url().should('equal', 'http://localhost:3000/auth/login')
    cy.get('[aria-describedby="password error"]')
      .should('exist')
      .and('be.visible')
      .and('contain.text', 'mismatch')
  })
})
