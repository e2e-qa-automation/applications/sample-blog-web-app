// / <reference types="cypress" />

// @use-case/user-registration @web
describe('Feature: Enable Users authentication', () => {
  // @persona/jordan @nominal
  it(
    `Scenario: On Web App, Users could register for new account on /auth/register page
    When Jordan registers for a new account
    Then Jordan is redirected to the home page
    And Jordan is able to see latest posts on home page`,
    () => {
      cy.intercept('POST', '/auth/register', {
        statusCode: 201,
        body: {
          user: {
            userId: 1,
          },
          token: 'qwertyuiop',
        },
      }).as('api-register')
      cy.register('another-username', 'another-password')
      cy.url().should('equal', 'http://localhost:3000/')
      cy.get('[aria-describedby="posts list"]').should('exist')
    })

  // @persona/jordan @rule/username-required
  it(
    `Scenario: On Web App, Users cannot register for new account without a username
    When Jordan registers for a new account and ommit username
    Then Jordan is still on register page
    And Jordan is warned that the username field is required`,
    () => {
      cy.intercept('POST', '/auth/register', {
        statusCode: 400,
        body: {
          code: 'rule/username-required',
          details: 'username is required for registration',
        },
      }).as('api-register')
      cy.register(undefined, 'another-password')
      cy.url().should('equal', 'http://localhost:3000/auth/register')
      cy.get('[aria-describedby="username error"]')
        .should('exist')
        .and('be.visible')
        .and('contain.text', 'required')
    })

  // @persona/jordan @rule/password-required
  it(
    `Scenario: On Web App, Users cannot register for new account without a password
    When Jordan registers for a new account and ommit password
    Then Jordan is still on register page
    And Jordan is warned that the password field is required`,
    () => {
      cy.intercept('POST', '/auth/register', {
        statusCode: 400,
        body: {
          code: 'rule/password-required',
          details: 'password is required for registration',
        },
      }).as('api-register')
      cy.register('another-username', undefined)
      cy.url().should('equal', 'http://localhost:3000/auth/register')
      cy.get('[aria-describedby="password error"]')
        .should('exist')
        .and('be.visible')
        .and('contain.text', 'required')
    })

  // @persona/jordan @rule/username-unique
  it(
    `Scenario: On Web App, Users cannot register for new account with existing username
    When Jordan registers for a new account and provides same username as Kevin
    Then Jordan is still on register page
    And Jordan is warned that the username is already registered`,
    () => {
      cy.intercept('POST', '/auth/register', {
        statusCode: 409,
        body: {
          code: 'rule/username-unique',
          details: 'username is already registered',
        },
      }).as('api-register')
      cy.register(undefined, 'another-password')
      cy.url().should('equal', 'http://localhost:3000/auth/register')
      cy.get('[aria-describedby="username error"]')
        .should('exist')
        .and('be.visible')
        .and('contain.text', 'already registered')
    })

  // @persona/jordan @rule/password-length @rule/password-upper-cases @rule/password-digits @rule/password-special-chars
  const testCases = [
    ['rule/password-length', 'password length must be 8 or more', 'pass'],
    ['rule/password-upper-cases', 'password must include at least one upper case character', 'passswwwwwoooorrrrddd'],
    ['rule/password-digits', 'password must include at least one digit character', 'PppAaaSssWwwOooRrrDdd'],
    ['rule/password-special-chars', 'password must include at least one special character', 'Ppp1Aaa3Sss4Www5Ooo6Rrr7Ddd8'],
  ]
  testCases.forEach(([rule, details, passwordCandidate]) => {
    it(
      `Scenario: On Web App, Users cannot register for new account with existing username
      When Jordan registers for a new account and provides an invalid password (${rule})
      Then Jordan is still on register page
      And Jordan is warned that the password rule ${rule} is not respected`,
      () => {
        cy.intercept('POST', '/auth/register', {
          statusCode: 400,
          body: {
            code: rule,
            details,
          },
        }).as('api-register')
        cy.register('another-username', passwordCandidate)
        cy.url().should('equal', 'http://localhost:3000/auth/register')
        cy.get('[aria-describedby="password error"]')
          .should('exist')
          .and('be.visible')
          .and('have.text', details)
      })
  })
})
