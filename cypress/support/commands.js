// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

/**
 *
 */
Cypress.Commands.add('login', (username, password) => {
  cy.visit('/auth/login')
  if (username) cy.get('[aria-describedby="username field"]').type(username)
  if (password) cy.get('[aria-describedby="password field"]').type(password)
  cy.get('[aria-describedby="submit login button"]').click()
})

/**
 *
 */
Cypress.Commands.add('register', (username, password) => {
  cy.visit('/auth/register')
  if (username) cy.get('[aria-describedby="username field"]').type(username)
  if (password) cy.get('[aria-describedby="password field"]').type(password)
  cy.get('[aria-describedby="submit registration button"]').click()
})
