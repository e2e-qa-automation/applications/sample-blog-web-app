import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  user: undefined,
  token: undefined,
}

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload
    },
    setToken: (state, action) => {
      state.token = action.payload
    },
    rmUser: (state, action) => {
      state.user = undefined
    },
    rmToken: (state, action) => {
      state.token = undefined
    },
  },
})

// Action creators are generated for each case reducer function
export const { setUser, setToken, rmUser, rmToken } = authSlice.actions

export default authSlice.reducer
