import classNames from 'classnames'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'

import Header from '../../components/Header'
import { blogApi } from '../../lib/blog-api'
import { setUser, setToken } from '../../state/slices/authSlice'

const Register = () => {
  const [username, setUsername] = useState('')
  const [usernameError, setUsernameError] = useState('')
  const [password, setPassword] = useState('')
  const [passwordError, setPasswordError] = useState('')
  // const [isLoading, setIsLoading] = useState(false)
  const [formError, setFormError] = useState('')
  const dispatch = useDispatch()
  const router = useRouter()

  const onPasswordChangeHandler = (e) => {
    setPassword(e.target.value)
  }

  const onSubmitFormHandler = (e) => {
    e.preventDefault()
    console.log('submitting registration form')
    blogApi.register(username, password)
      .then((response) => {
        console.log('received a response from API')
        if (response.status === 201) {
          console.log('API response is CREATED')
          console.log('registration success')
          dispatch(setUser(response.data.user))
          dispatch(setToken(response.data.token))
          localStorage.setItem('user_id', response.data.user.user_id)
          localStorage.setItem('token', response.data.token)
          router.push('/')
        } else {
          console.log('API response is', response.status)
          console.log('registration failure')
          if (response.data.code) {
            console.log('code attributes found in response payload')
            if (response.data.code.includes('username')) {
              console.log('API error is about username field')
              setUsernameError(response.data.details)
            } else if (response.data.code.includes('password')) {
              console.log('API error is about password field')
              setPasswordError(response.data.details)
            } else {
              console.log('API error is unexpected')
              setFormError('An unexpected error occurs... Please try again later.')
            }
          } else {
            console.log('code attributes not found in response payload')
            setFormError('An unexpected error occurs... Please try again later.')
          }
        }
      })
  }

  const onUsernameChangeHandler = (e) => {
    setUsername(e.target.value)
  }

  return (
    <>
      <Header />
      <main>
        <article>
          <form onSubmit={onSubmitFormHandler}>
            <div className='field-bloc'>
              <label>Username</label>
              <input
                aria-describedby='username field'
                className={classNames({ 'field-with-error': usernameError })}
                id='username'
                name='username'
                onChange={onUsernameChangeHandler}
                type='text'
                value={username}
              />
              {usernameError && <span className='field-error' aria-describedby='username error'>{usernameError}</span>}
            </div>
            <div className='field-bloc'>
              <label>Password</label>
              <input
                aria-describedby='password field'
                className={classNames({ 'field-with-error': passwordError })}
                id='password'
                name='password'
                onChange={onPasswordChangeHandler}
                type='password'
                value={password}
              />
              {passwordError && <span className='field-error' aria-describedby='password error'>{passwordError}</span>}
            </div>
            <div className='form-errors-bloc'>
              {formError && <span className='form-error' aria-describedby='form error'>{formError}</span>}
            </div>
            <div className='form-actions-bloc'>
              <button aria-describedby='submit registration button' type="submit">Submit</button>
            </div>
          </form>
        </article>
      </main>
      <footer></footer>
    </>
  )
}

export default Register
