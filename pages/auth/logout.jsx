import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import { useDispatch } from 'react-redux'

import { rmUser, rmToken } from '../../state/slices/authSlice'

const Logout = () => {
  const dispatch = useDispatch()
  const router = useRouter()

  useEffect(() => {
    dispatch(rmUser())
    dispatch(rmToken())
    localStorage.removeItem('user_id')
    localStorage.removeItem('token')
    router.push('/')
  })

  return (
    <></>
  )
}

export default Logout
