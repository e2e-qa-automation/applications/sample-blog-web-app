import classNames from 'classnames'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'

import Header from '../../components/Header'
import { blogApi } from '../../lib/blog-api'
import { setUser, setToken } from '../../state/slices/authSlice'

const Login = () => {
  const [username, setUsername] = useState('')
  const [usernameError, setUsernameError] = useState('')
  const [password, setPassword] = useState('')
  const [passwordError, setPasswordError] = useState('')
  // const [isLoading, setIsLoading] = useState(false)
  const [formError, setFormError] = useState('')
  const dispatch = useDispatch()
  const router = useRouter()

  const onPasswordChangeHandler = (e) => {
    setPassword(e.target.value)
  }

  const onSubmitFormHandler = (e) => {
    e.preventDefault()
    console.log('submit...')
    blogApi.login(username, password)
      .then((response) => {
        if (response.status === 200) {
          console.log('login success')
          dispatch(setUser(response.data.user))
          dispatch(setToken(response.data.token))
          router.push('/')
        } else if (response.status === 404) {
          setUsernameError(response.data.details)
        } else {
          console.log('login failure')
          if (response.data.code) {
            if (response.data.code.includes('user-exists')) setUsernameError(response.data.details)
            else if (response.data.code.includes('username')) setUsernameError(response.data.details)
            else if (response.data.code.includes('password')) setPasswordError(response.data.details)
            else {
              setFormError('An unexpected error occurs... Please try again later.')
            }
          } else {
            setFormError('An unexpected error occurs... Please try again later.')
          }
        }
      })
  }

  const onUsernameChangeHandler = (e) => {
    setUsername(e.target.value)
  }

  return (
    <>
      <Header />
      <main>
        <article>
          <form onSubmit={onSubmitFormHandler}>
            <div className='field-bloc'>
              <label>Username</label>
              <input
                aria-describedby='username field'
                className={classNames({ 'field-with-error': usernameError })}
                id='username'
                name='username'
                onChange={onUsernameChangeHandler}
                type='text'
                value={username}
              />
              {usernameError && <span className='field-error' aria-describedby='username error'>{usernameError}</span>}
            </div>
            <div className='field-bloc'>
              <label>Password</label>
              <input
                aria-describedby='password field'
                className={classNames({ 'field-with-error': passwordError })}
                id='password'
                name='password'
                onChange={onPasswordChangeHandler}
                type='password'
                value={password}
              />
              {passwordError && <span className='field-error' aria-describedby='password error'>{passwordError}</span>}
            </div>
            <div className='form-errors-bloc'>
              {formError && <span className='form-error' aria-describedby='form error'>{formError}</span>}
            </div>
            <div className='form-actions-bloc'>
              <button aria-describedby='submit login button' type="submit">Submit</button>
            </div>
          </form>
        </article>
      </main>
      <footer></footer>
    </>
  )
}

export default Login
