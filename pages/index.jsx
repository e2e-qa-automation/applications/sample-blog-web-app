import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'

import Header from '../components/Header'

const IndexPage = () => {
  const user = useSelector((state) => state.auth.user)

  useEffect(() => {
    if (user) {
      console.log('Congratulations on logging in, we\'ll load the blog posts for you...')
    } else {
      console.log('It seems that you are not logged in yet...')
    }
  }, [user])

  return (
    <>
      <Header />
      <main>
        <h1>The Blog app</h1>
        {!user && (
          <article aria-describedby='should register article'>
            <h2>Create your account on this blog</h2>
          </article>
        )}
        {user && (
          <>
            <article aria-describedby='welcome article'>
              <h2>Welcome to this blog</h2>
            </article>
            <article aria-describedby='posts list article'>
              <ul aria-describedby='posts list'></ul>
            </article>
          </>
        )}
      </main>
      <footer>

      </footer>
    </>
  )
}

export default IndexPage
