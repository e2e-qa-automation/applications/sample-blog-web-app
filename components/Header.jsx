import Link from 'next/link'
import React from 'react'
import { useSelector } from 'react-redux'

const Header = () => {
  const user = useSelector((state) => state.auth.user)

  return (
    <header>
      <nav>
        {!user && (
          <ul>
            <li>
              <Link href="/auth/login">
                <a aria-describedby='login link'>Login</a>
              </Link>
            </li>
            <li>
              <Link href="/auth/register">
                <a aria-describedby='register link'>Register</a>
              </Link>
            </li>
          </ul>
        )}
        {user && (
          <ul>
            <li>
              <Link href="/auth/logout">
                <a aria-describedby='logout link'>Logout</a>
              </Link>
            </li>
          </ul>
        )}
      </nav>
    </header>
  )
}

export default Header
