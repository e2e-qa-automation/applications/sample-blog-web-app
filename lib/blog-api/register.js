import environment from '../../environment'
import { axiosPromise } from '../axios/axiosPromise'

export const register = (username, password) => axiosPromise(
  'post',
  environment.apiBaseUrl + '/auth/register',
  {
    username,
    password,
  },
  {
    headers: {
      'Content-Type': 'application/json',
    },
  },
)
