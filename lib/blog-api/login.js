import environment from '../../environment'
import { axiosPromise } from '../axios/axiosPromise'

export const login = (username, password) => axiosPromise(
  'post',
  environment.apiBaseUrl + '/auth/login',
  {
    username,
    password,
  },
  {
    headers: {
      'Content-Type': 'application/json',
    },
  },
)
