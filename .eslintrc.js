module.exports = {
  'env': {
    'browser': true,
    'es2021': true,
    'node': true,
  },
  'extends': [
    'google',
    'plugin:react/recommended',
    'plugin:import/recommended',
    'next/core-web-vitals',
  ],
  'parserOptions': {
    'ecmaFeatures': {
      'jsx': true,
    },
    'ecmaVersion': 'latest',
    'sourceType': 'module',
  },
  'plugins': [
    'jsx-a11y',
    'import',
    'react',
  ],
  'rules': {
    'camelcase': 'off',
    'import/newline-after-import': ['error', { 'count': 1 }],
    'import/order': ['error', { 'alphabetize': { 'order': 'asc' }, 'groups': ['builtin', 'external', 'internal'], 'newlines-between': 'always' }],
    'indent': ['error', 2],
    'key-spacing': ['error', { 'afterColon': true }],
    'keyword-spacing': ['error', { 'after': true }],
    'linebreak-style': ['error', 'unix'],
    'max-len': 'off',
    'new-cap': 'off',
    'no-invalid-this': 'off',
    'object-curly-spacing': ['error', 'always'],
    'quotes': ['error', 'single'],
    'require-jsdoc': 'off',
    'semi': ['error', 'never'],
    'react/prop-types': [2],
  },
}
